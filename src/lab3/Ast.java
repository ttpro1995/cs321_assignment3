package lab3;



import java.util.*;
import java.io.PrintWriter;
import java.lang.reflect.Field;

abstract class AST {
     public abstract void printTree(PrintWriter p, int indent);
     protected int printName(PrintWriter p, int indent) {
        this.doIndent(p, indent);
        p.println(this.getClass().getSimpleName().toString());
        return indent + 4;
    }

    protected void doIndent(PrintWriter p, int indent) {
        int k = 0;
        while (k < indent) {
            p.print(" ");
            ++k;
        }
    }

    protected void printTerm(PrintWriter p, int indent, int token) {
        this.doIndent(p, indent);
        p.println(this.getTokenName(token));
    }

    protected void printTerm(PrintWriter p, int indent, int token, String val) {
        this.doIndent(p, indent);
        p.println(String.valueOf(this.getTokenName(token)) + " (" + val + ")");
    }

    protected String getTokenName(int token) {
        try {
            Field[] classFields = sym.class.getFields();
            int i = 0;
            while (i < classFields.length) {
                if (classFields[i].getInt(null) == token) {
                    return classFields[i].getName();
                }
                ++i;
            }
        }
        catch (Exception e) {
            e.printStackTrace(System.err);
        }
        return "UNKNOWN TOKEN";
    }
}

class Program extends AST {
    public Program(DeclList L) {
        myDeclList = L;
    }
	
    private DeclList myDeclList;

    @Override
    public void printTree(PrintWriter p, int indent) {
           indent = this.printName(p, indent);
        this.myDeclList.printTree(p, indent);
    }
    
        
}

class DeclList extends AST {
    public DeclList(LinkedList<?> S) {
        myDecls = S;
    }

    protected LinkedList<?> myDecls;

    @Override
    public void printTree(PrintWriter p, int indent) {
        Iterator it = this.myDecls.iterator();
        try {
            indent = this.printName(p, indent);
            while (it.hasNext()) {
                ((Decl)it.next()).printTree(p, indent);
            }
        }
        catch (NoSuchElementException ex) {
            System.err.println("unexpected NoSuchElementException in DeclListNode.unparse");
            System.exit(-1);
        }
    }
}

class StructDeclList extends DeclList {
    public StructDeclList(LinkedList<?> S) {
        super(S);
    }
}

class FormalsList extends AST {
    public FormalsList(LinkedList<?> S) {
        myFormals = S;
    }

    private LinkedList<?> myFormals;

    @Override
    public void printTree(PrintWriter p, int indent) {
     Iterator it = this.myFormals.iterator();
        try {
            indent = this.printName(p, indent);
            if (it.hasNext()) {
                ((FormalDecl)it.next()).printTree(p, indent);
            }
            while (it.hasNext()) {
                this.printTerm(p, indent, sym.COMMA);
                ((FormalDecl)it.next()).printTree(p, indent);
            }
        }
        catch (NoSuchElementException ex) {
            System.err.println("unexpected NoSuchElementException in FormalsListNode.unparse");
            ex.printStackTrace();
            System.exit(-1);
        }
    }
}

class FuncBody extends AST {
    public FuncBody(DeclList declList, StmtList stmtList) {
        myDeclList = declList;
        myStmtList = stmtList;
    }

    private DeclList myDeclList;
    private StmtList myStmtList;

    @Override
    public void printTree(PrintWriter p, int indent) {
       indent = this.printName(p, indent);
        this.printTerm(p, indent, sym.LCURLY);
        this.myDeclList.printTree(p, indent);
        this.myStmtList.printTree(p, indent);
        this.printTerm(p, indent, sym.RCURLY);
    }
}

class StmtList extends AST {
    public StmtList(LinkedList<?> S) {
        myStmts = S;
    }

    private LinkedList<?> myStmts;

    @Override
    public void printTree(PrintWriter p, int indent) {
           Iterator it = this.myStmts.iterator();
        try {
            indent = this.printName(p, indent);
            while (it.hasNext()) {
                ((Stmt)it.next()).printTree(p, indent);
            }
        }
        catch (NoSuchElementException ex) {
            System.err.println("unexpected NoSuchElementException in StmtListNode.unparse");
            ex.printStackTrace();
            System.exit(-1);
        }
    }
}

abstract class Decl extends AST {

}

class VarDecl extends Decl {
    public VarDecl(Type type, ID id) {
        myType = type;
        myId = id;
        intLiteral = -1;
    }
    
    public VarDecl(Type type, ID id, int intLiteral){
        this.myType = type;
        this.myId = id;
        this.intLiteral = intLiteral;
    }

    private Type      myType;
    private ID        myId;
    private int       intLiteral;
    
    @Override
    public void printTree(PrintWriter p, int indent) {
     indent = this.printName(p, indent);
        this.myType.printTree(p, indent);
        this.myId.printTree(p, indent);
        //array case
        if (intLiteral >-1){
            this.printTerm(p, indent, sym.LSQBRACKET);
            this.printTerm(p,indent,sym.INTLITERAL,Integer.toString(intLiteral));
            this.printTerm(p, indent, sym.RSQBRACKET);
        }
        //
        this.printTerm(p, indent, sym.SEMICOLON);
    }
}

class FuncDef extends Decl {
    public FuncDef(Type type, int numPtr, ID id, FormalsList formalList,
            FuncBody body) {
        myType = type;
        myId = id;
        myFormalsList = formalList;
        myBody = body;
    }

    private Type        myType;
    private ID          myId;
    private FormalsList myFormalsList;
    private FuncBody    myBody;

    @Override
    public void printTree(PrintWriter p, int indent) {
       indent = this.printName(p, indent);
        this.myType.printTree(p, indent);
        this.myId.printTree(p, indent);
        this.printTerm(p, indent, sym.LPAREN);
        this.myFormalsList.printTree(p, indent);
        this.printTerm(p, indent, sym.LPAREN);
        this.myBody.printTree(p, indent);
    }
}

class FormalDecl extends Decl {
    public FormalDecl(Type type, ID id, int ptrs) {
        myType = type;
        myId = id;
    }

    private Type myType;
    private ID   myId;

    @Override
    public void printTree(PrintWriter p, int indent) {
          indent = this.printName(p, indent);
        this.myType.printTree(p, indent);
        this.myId.printTree(p, indent);
    }
}

// **********************************************************************
// Type
// **********************************************************************
abstract class Type extends AST {

    abstract public String name();
    
    @Override
    public void printTree(PrintWriter p, int indent) {
        this.doIndent(p, indent);
        p.println("Type (" + this.name() + ")");
    }
}

class IntType extends Type {
    public IntType() {
    }

    public String name() {
        return "INT";
    }

    
}

class VoidType extends Type {
    public VoidType() {
    }

    public String name() {
        return "VOID";
    }

    
}

// **********************************************************************
// Expr
// **********************************************************************

abstract class Expr extends AST {

}

class IntLiteral extends Expr {
    public IntLiteral(int intVal) {
        myIntVal = intVal;
    }

    private int myIntVal;

    @Override
    public void printTree(PrintWriter p, int indent) {
       this.printTerm(p, indent, sym.INTLITERAL, Integer.toString(this.myIntVal));
    }
}

class ID extends Expr {
    public ID(String strVal) {
        myStrVal = strVal;
    }

    public String getName() {
        return myStrVal;
    }

    private String myStrVal;

    @Override
    public void printTree(PrintWriter p, int indent) {
       this.printTerm(p, indent, sym.ID, this.getName());
    }
}

abstract class BinaryExpr extends Expr {
    public BinaryExpr(Expr exp1, Expr exp2) {
        myExp1 = exp1;
        myExp2 = exp2;
    }

    protected Expr myExp1;
    protected Expr myExp2;
    
     public void printTree(PrintWriter p, int indent, int token) {
        indent = this.printName(p, indent);
        this.myExp1.printTree(p, indent);
        this.printTerm(p, indent, token);
        this.myExp2.printTree(p, indent);
    }
}

class PlusExpr extends BinaryExpr {
    public PlusExpr(Expr exp1, Expr exp2) {
        super(exp1, exp2);
    }

    @Override
    public void printTree(PrintWriter p, int indent) {
        this.printTree(p, indent, sym.PLUS);
    }
}

class MinusExpr extends BinaryExpr {
    public MinusExpr(Expr exp1, Expr exp2) {
        super(exp1, exp2);
    }

    @Override
    public void printTree(PrintWriter p, int indent) {
        this.printTree(p, indent, sym.MINUS);
    }
}

// **********************************************************************
// Stmt
// **********************************************************************

abstract class Stmt extends AST {

}

class AssignStmt extends Stmt {
    public AssignStmt(Expr lhs, Expr exp) {
        myLhs = lhs;
        myExp = exp;
    }

    private Expr myLhs;
    private Expr myExp;

    @Override
    public void printTree(PrintWriter p, int indent) {
       indent = this.printName(p, indent);
        this.myLhs.printTree(p, indent);
        this.printTerm(p, indent, sym.ASSIGN);
        this.myExp.printTree(p, indent);
        this.printTerm(p, indent, sym.SEMICOLON);
    }
}

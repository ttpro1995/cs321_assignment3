
import java_cup.runtime.*;
import java.util.*;


parser code {: 

public void syntax_error(Symbol curToken) {
    if (curToken.value == null) {
       Errors.fatal(0,0, "SYNTAX ERROR at end of file");
    }
    else { 
       MySymbol cur = (MySymbol)curToken;
       Errors.fatal(cur.getLine(), cur.getColumn(),
            "SYNTAX ERROR while reading token " + ((Scanner)this.getScanner()).getTokName(cur.sym) + " ("+ ((Scanner)this.getScanner()).yytext() + ")");
    } 
    System.exit(-1);
} 

:};


/* Terminals */

terminal            INT;
terminal            BOOL;
terminal            VOID;
terminal Boolean    TRUE;
terminal Boolean    FALSE;
terminal            NULL;
terminal            IF;
terminal            ELSE;
terminal            WHILE;
terminal            FOR;
terminal            RETURN;
terminal String     ID;
terminal Integer    INTLITERAL;
terminal String     STRINGLITERAL;
terminal Double     DOUBLELITERAL;
terminal            LCURLY;
terminal            RCURLY;
terminal            LSQBRACKET;
terminal            RSQBRACKET; 
terminal            LPAREN;
terminal            RPAREN;
terminal            COMMA;
terminal            ASSIGN;
terminal            SEMICOLON;
terminal            PLUS;
terminal            MINUS;
terminal            TIMES;
terminal            DIVIDE;
terminal            NOT;
terminal            AND;
terminal            OR;
terminal            EQUALS;
terminal            NOTEQUALS;
terminal            LESS;
terminal            GREATER;
terminal            LESSEQ;
terminal            GREATEREQ;
terminal            ADDROF;
terminal            SIZEOF;
terminal            PERIOD;
terminal            STRUCT;
terminal            PLUSEQL;
terminal            MINUSEQL;
terminal            TIMESEQL;
terminal            DIVEQL;
terminal            PERCENT;

/* Nonterminals */

non terminal Program      program;
non terminal LinkedList       declList;
non terminal Decl           decl;
non terminal LinkedList       varDeclList;
non terminal VarDecl      varDecl;
non terminal FuncDef       funcDef; 
non terminal FormalsList  formals; 
non terminal LinkedList       formalsList; 
non terminal FormalDecl   formalDecl;
non terminal FuncBody       funcBody;
non terminal LinkedList       stmtList;
non terminal Stmt         stmt;
non terminal Expr          exp;
non terminal Expr          term;
non terminal Type           type;
non terminal Expr          loc;
non terminal ID           id;
non terminal Stmt         assignStmt;

non terminal BinaryExpr     binaryExpr;


precedence left PLUS, MINUS;

start with program; 

/*
 * grammar with actions
 */
 
program ::= declList: d {: RESULT = new Program(new DeclList(d)); :}
          ;

declList ::= declList: dl decl: d {: dl.addLast(d); RESULT = dl; :}
           | /* epsilon */ {: RESULT = new LinkedList(); :}
           ;

decl ::= varDecl: v {: RESULT = v; :}
       | funcDef: f {: RESULT = f; :}
       ;

varDecl ::= type: t id: i SEMICOLON
             {: RESULT = new VarDecl(t, i); :}
             | type: t id: i LSQBRACKET INTLITERAL: lit RSQBRACKET SEMICOLON
            {: RESULT = new VarDecl(t, i, lit); :}
          ;
                
funcDef ::= type: t id: i formals: f funcBody: body {: RESULT = new FuncDef(t, 0, i, f, body); :}
         ;

formals ::= LPAREN RPAREN {: RESULT = new FormalsList(new LinkedList()); :}
          | LPAREN formalsList: l RPAREN {: RESULT = new FormalsList(l); :}
          ;

formalsList ::= formalDecl: f {: RESULT = new LinkedList(); RESULT.addLast(f); :}
              | formalDecl: f COMMA formalsList: l {: l.addFirst(f); RESULT = l; :}
              ;

formalDecl  ::= type:t id: i {: RESULT = new FormalDecl(t, i, 0); :}
              ;

funcBody ::= LCURLY varDeclList: v stmtList: s RCURLY 
              {: RESULT = new FuncBody(new DeclList(v), new StmtList(s)); :}
         ;

varDeclList ::= varDeclList: l varDecl: var {: l.addLast(var); RESULT = l; :}
              | /* epsilon */ {: RESULT = new LinkedList(); :}
              ;
              
stmtList ::= stmtList: l stmt: s {: l.addLast(s); RESULT = l; :}
          |  /* epsilon */ {: RESULT = new LinkedList(); :}
          ;

stmt ::= assignStmt:a SEMICOLON {: RESULT = a; :}
      ;

assignStmt ::= loc: l ASSIGN exp: e {: RESULT = new AssignStmt(l, e); :}
         ;

exp ::= binaryExpr: b {: RESULT = b; :}
      | term: t {: RESULT = t; :}
      ;
      
binaryExpr ::= exp: e1 PLUS exp: e2 {: RESULT = new PlusExpr(e1, e2); :}
        | exp: e1 MINUS exp: e2 {: RESULT = new MinusExpr(e1, e2); :}
        ;
        
term ::= loc: l {: RESULT = l; :}
       | INTLITERAL: i {: RESULT = new IntLiteral(i); :}
       ;
         
type ::= INT {: RESULT = new IntType(); :}
       | VOID {: RESULT = new VoidType(); :} 
       ;

loc ::= id: i {: RESULT = i; :}
      ;
      
id ::= ID: i {: RESULT = new ID(i); :}
     ; 
Program
    DeclList
        VarDecl
            Type (INT)
            ID (meow)
            LSQBRACKET
            INTLITERAL (100)
            RSQBRACKET
            SEMICOLON
        FuncDef
            Type (VOID)
            ID (testMeow)
            LPAREN
            FormalsList
                FormalDecl
                    Type (INT)
                    ID (Pusheen)
                COMMA
                FormalDecl
                    Type (INT)
                    ID (Kitty)
            LPAREN
            FuncBody
                LCURLY
                DeclList
                StmtList
                RCURLY
        FuncDef
            Type (VOID)
            ID (main)
            LPAREN
            FormalsList
            LPAREN
            FuncBody
                LCURLY
                DeclList
                    VarDecl
                        Type (INT)
                        ID (a)
                        SEMICOLON
                    VarDecl
                        Type (INT)
                        ID (b)
                        SEMICOLON
                    VarDecl
                        Type (INT)
                        ID (c)
                        SEMICOLON
                    VarDecl
                        Type (INT)
                        ID (d)
                        SEMICOLON
                StmtList
                    AssignStmt
                        ID (c)
                        ASSIGN
                        PlusExpr
                            ID (a)
                            PLUS
                            ID (b)
                        SEMICOLON
                    AssignStmt
                        ID (d)
                        ASSIGN
                        MinusExpr
                            ID (a)
                            MINUS
                            ID (b)
                        SEMICOLON
                RCURLY
